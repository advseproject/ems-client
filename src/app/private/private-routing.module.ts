import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WlayoutComponent} from './wlayout/wlayout.component';
import {ExamRegistrationComponent} from './exam-registration/exam-registration.component';
import {ExamComponent} from './exam/exam.component';

const routes: Routes = [
    { path: '', component: WlayoutComponent, children: [
            { path: '', component: ExamRegistrationComponent },
            { path: 'exam/:id', component: ExamComponent }
        ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrivateRoutingModule { }
