import { Component, OnInit } from '@angular/core';
import {ExamRegistration} from "../../model/exam-registration";
import {ExamManagerService} from "../exam-manager.service";
import {KeycloakService} from 'keycloak-angular';

@Component({
  selector: 'app-exam-registration',
  templateUrl: './exam-registration.component.html',
  styleUrls: ['./exam-registration.component.scss']
})
export class ExamRegistrationComponent implements OnInit {

  examRegistrationlist: any;

  username: any;
  userData: any;

  constructor(private examManager: ExamManagerService, private keycloak: KeycloakService) {
    console.log(this.keycloak.getUsername());
    this.keycloak.loadUserProfile().then(data => {
      let obs = this.examManager.getExamRegistrationsByUsername(data.username);
      this.username = data.username;
      obs.subscribe( data => {
        this.examRegistrationlist = data;

        this.examManager.getAllUsers().subscribe(data => {
          console.log(data);
          this.userData = data;
          this.userData = this.userData.find(x => x.username == this.username);
          let id = this.userData.id;
          this.examRegistrationlist = this.examRegistrationlist.filter( x => x.examinee == this.userData.id);
          this.examRegistrationlist.sort( (a,b) => {
            let aDate = new Date(a.examDate);
            let bDate = new Date(b.examDate);
            if (a.examDate == null){
              return -1;
            }
            if (aDate > bDate){
              return -1;
            } else if (aDate < bDate){
              return 1;
            } else {
              return 0;
            }
          });
          console.log(id);
        });


      });
    })
  }

  ngOnInit() {

  }






}
