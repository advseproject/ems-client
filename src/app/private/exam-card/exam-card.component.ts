import {Component, Input, OnInit} from '@angular/core';
import {ExamManagerService} from '../exam-manager.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-exam-card',
  templateUrl: './exam-card.component.html',
  styleUrls: ['./exam-card.component.scss']
})
export class ExamCardComponent implements OnInit {

  @Input() examreg: any;

  public currentDate: any;

  constructor(private examManager: ExamManagerService, private router: Router) { }

  ngOnInit() {
    this.currentDate = new Date().getTime();
    console.log(this.currentDate);
    console.log(new Date(this.examreg.examDate).getTime());
    console.log(Date.parse(this.examreg.examDate));
  }

  startExam(){
    this.router.navigate(['exam', this.examreg.id ]);
    // this.examManager.takeExam(this.examreg.id).subscribe(data => {
    //   console.log(data);
    //
    // }, error1 => {
    //     console.log(error1);
    //   }
    // );
  }

}
