import { Component, OnInit } from '@angular/core';
import {ExamManagerService} from '../exam-manager.service';
import {Exam} from '../../model/exam';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable, timer} from 'rxjs';
import {map, take, tap} from 'rxjs/operators';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {

  // exam: Exam;
  // currentQuestion: number = 0;
  // currentQuestionText: string;
  // choices: any;
  // defaultChoice: any;

  public questions: any;
  public qIndex: number;
  private examId: any;
  public exam: any;
  public diff: any;

  counter$: Observable<number>;
  count = 1800000;
  minute = 60000;


  constructor(private examManager: ExamManagerService, private route: ActivatedRoute, private router: Router) {



    this.route.params.subscribe( params => {
      this.examManager.takeExam(params['id']).subscribe((data) => {
        this.questions = data;
        // console.log(this.questions.size)
        this.qIndex = 0;
        this.examId = params['id'];

        this.examManager.getExam(params['id']).subscribe( (data) => {
          this.exam = data;
          // let today: any = new Date();
          // // console.log(today);
          // let deadline: any = new Date(this.exam.examDate);
          // deadline = new Date(deadline.getTime() + 5*60*60*1000)
          // // console.log(Christmas);
          // let diffMs: any = (deadline - today); // milliseconds between now & Christmas
          // // let diffDays = Math.floor(diffMs / 86400000); // days
          // // let diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
          // let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
          //
          // // console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes until Christmas 2009 =)");
          // this.diff = diffMins;
          // console.log("first");
          // console.log(today);
          // console.log(deadline);
          // console.log(diffMs);
          // console.log(diffMins);

          this.counter$ = timer(0,60000).pipe(
            take(this.count),
            map(() => this.count = this.count - this.minute),
            tap( val => {
              let today: any = new Date();
              // console.log(today);
              let deadline: any = new Date(this.exam.examDate);
              deadline = new Date(deadline.getTime() + 5*60*60*1000)
              // console.log(Christmas);
              let diffMs: any = (deadline - today); // milliseconds between now & Christmas
              // let diffDays = Math.floor(diffMs / 86400000); // days
              // let diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
              let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
              // console.log(diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes until Christmas 2009 =)");
              this.diff = diffMins;

              // console.log("second");
              // console.log(today);
              // console.log(deadline);
              // console.log(diffMs);
              // console.log(diffMins);
            })
          );
        });
      });

    });
  }

  choose(value: string) {

    this.questions[this.qIndex].choices[value].chosen = !this.questions[this.qIndex].choices[value].chosen;
  }

  ngOnInit() {
    // this.currentQuestionText = this.exam.questions[this.currentQuestion].text;
    // this.choices = this.exam.questions[this.currentQuestion].choices;
  }

  public next(): void {
    if (this.qIndex < this.questions.length -1){
      this.submitQuestion()
      this.qIndex = this.qIndex + 1;

      // this.currentQuestionText = this.exam.questions[this.currentQuestion].text;
      // this.choices = this.exam.questions[this.currentQuestion].choices;
    }
  }

  public prev(): void {
    if (this.qIndex > 0) {
      this.submitQuestion();
      this.qIndex -= 1;

      // this.currentQuestionText = this.exam.questions[this.currentQuestion].text;
      // this.choices = this.exam.questions[this.currentQuestion].choices;
    }
  }

  public submitExam(): void {
    this.examManager.submitExam(this.examId).subscribe( data => {
      this.router.navigate(['/'])
    })
  }

  public submitQuestion(): any {
    let result = {
      "examId": this.examId,
      "questionId": this.questions[this.qIndex].id,
      "choiceEmsDtos": this.questions[this.qIndex].choices
    };
    console.log(result);
    this.examManager.submitQuestion(result).subscribe( data => {
      console.log(data);
    })
}

  /**
   * Submitting the exam & redirecting to corresponding results
   */
  public submit(){
    let result = {
      "examId": this.examId,
      "questionId": this.questions[this.qIndex].id,
      "choiceEmsDtos": this.questions[this.qIndex].choices
    };
    console.log(result);
    this.examManager.submitQuestion(result).subscribe( data => {
      console.log(data);
      if(confirm("Are you sure to submit the exam?")) {
        this.submitExam();
      }
    })

  }

}
