import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { ExamRegistrationComponent } from './exam-registration/exam-registration.component';
import { ExamComponent } from './exam/exam.component';
import { WlayoutComponent } from './wlayout/wlayout.component';
import { ExamCardComponent } from './exam-card/exam-card.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';

@NgModule({
  declarations: [ExamRegistrationComponent, ExamComponent, WlayoutComponent, ExamCardComponent, NavigationComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    PrivateRoutingModule
  ]
})
export class PrivateModule { }
