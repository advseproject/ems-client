import {Choice} from './choice';

export class Question {
    text?: any;
    choices?: Array<Choice>;
    correctChoice?: Choice;
}
