import {Question} from './question';

export class Exam {
    questions: Array<Question>;
}
