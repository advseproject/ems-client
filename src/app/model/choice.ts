export class Choice {
    text: string;
    isSelected: boolean = false;
    id: number;
}
