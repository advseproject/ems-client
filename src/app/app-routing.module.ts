import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
    { path: '', redirectTo: '/',
      pathMatch: 'full' },
    { path: '', loadChildren: './private/private.module#PrivateModule', canActivate: [AuthGuard],  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
